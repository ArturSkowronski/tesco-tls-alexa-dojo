rm -f lambda/custom/package-lock.json

git reset --hard
git log --reverse --pretty=%H master | grep -A 1 $(git rev-parse HEAD) | tail -n1 | xargs git checkout
cd lambda/custom

npm install
cd ../../../