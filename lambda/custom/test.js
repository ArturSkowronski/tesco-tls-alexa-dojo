var TLSSDK = require("./client");
const city = "Welwyn Garden City";
        TLSSDK.getNearestLocationByCity(city)
            .then((result) => {
                console.log(result)
                console.log(`Hello From Tesco Location Service. Nearest Location in ${city}: ${result.name}. ${TLSSDK.humanReadableOpeningHours(result)}`);
            })