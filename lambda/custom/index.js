'use strict';
var Alexa = require("alexa-sdk");
var TLSSDK = require("./client");

// For detailed tutorial on how to making a Alexa skill,
// please visit us at http://alexa.design/build


exports.handler = function(event, context) {
    var alexa = Alexa.handler(event, context);
    alexa.registerHandlers(handlers);
    alexa.execute();
};

var handlers = {
    'LaunchRequest': function () {
        this.emit(':ask', `Hello From Tesco Location Service. What do you want to do?` );
    }, 
    'NearestLocationIntent': function () {
        if (!this.event.request.intent.slots.city.value) {
            this.emit(':elicitSlot', 'city', `Which city are you interested in?`, "Say the city");
            return 
        } else if (!this.event.request.intent.slots.category.value) {
            this.emit(':elicitSlot', 'category', `What category of location are you looking for?`, "What category?");
            return
        } else {
            const city = this.event.request.intent.slots.city.value
            const category = this.event.request.intent.slots.category.value
            TLSSDK.getByCategoryAndLocation(city, category)
                .then((result) => {
                    this.response.speak(`Nearest Location in ${city} for category ${category}: ${result.name}. ${TLSSDK.humanReadableOpeningHours(result)}`);
                    this.emit(':responseReady');
            })
         }   
    },
    'ShopInfoIntent': function () {
        const city = this.event.request.intent.slots.city.value;
        
        TLSSDK.getNearestLocationByCity(city)
            .then((result) => {
                this.response.speak(`Nearest Location in ${city}: ${result.name}. ${TLSSDK.humanReadableOpeningHours(result)}`);
                this.emit(':responseReady');
            })
    },
    'SessionEndedRequest' : function() {
        console.log('Session ended with reason: ' + this.event.request.reason);
    },
    'AMAZON.StopIntent' : function() {
        this.response.speak('Bye');
        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent' : function() {
        this.response.speak("You can try: 'alexa, tesco location service' to get greetings");
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent' : function() {
        this.response.speak('Bye');
        this.emit(':responseReady');
    },
    'Unhandled' : function() {
        this.response.speak("Sorry, There was problems with Skill");
    }
};
