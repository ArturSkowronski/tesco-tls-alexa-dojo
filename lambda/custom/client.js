const {TescoStoreLocationClient, StoreLocationQueryBuilder} = require('tesco-store-location-sdk').SDK
const moment = require('moment')
const _ = require('lodash')

const subscriptionKey = 'e425489a004548a2b8c33c83986a519e'
const TLSClient = TescoStoreLocationClient
.instance(subscriptionKey)

exports.getNearestLocationByCity = (location) => TLSClient
    .searchLocations(new StoreLocationQueryBuilder().ukTown(location).category('Store').build())
    .then((result) => {
        return result.results[0].location;
    })
    .catch((err) => {
        throw new Error(`Error from Tesco Store Location ${err}`)
    })

exports.getByCategoryAndLocation = (location, category) => TLSClient
    .searchLocations(new StoreLocationQueryBuilder().ukTown(location).category(capitalizeFirstLetter(category)).build())
    .then((result) => {
        return result.results[0].location;
    })
    .catch((err) => {
        throw new Error(`Error from Tesco Store Location ${err}`)
    })

const date = (hour) => {
    return moment(`${hour.substr(0, 2)}:${hour.substr(2)}`, ["HH"]).format("hh A")
}

const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

exports.humanReadableOpeningHours = (location) => {
    if(!location.openingHours || !location.openingHours[0]) {
        return ""; 
    } else {
        const hours = _.values(location.openingHours[0].standardOpeningHours)[moment().day() -1]
        if (hours.isOpen) {
            if (hours.open && hours.close){
                return `This shop today is opened between 
                    <say-as interpret-as="time">${date(hours.open)}</say-as> and 
                    <say-as interpret-as="time">${date(hours.close)}</say-as>.`
            } else {
                return `This shop is opened 24/7`
            }
        } else {
            return "This shop is closed today."
        }
    }
}